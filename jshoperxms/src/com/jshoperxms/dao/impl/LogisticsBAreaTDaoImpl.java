package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.LogisticsBAreaTDao;
import com.jshoperxms.entity.LogisticsbusinessareaT;


@Repository("logisticsBAreaTDao")
public class LogisticsBAreaTDaoImpl extends BaseTDaoImpl<LogisticsbusinessareaT> implements LogisticsBAreaTDao {

	private static final Logger log = LoggerFactory.getLogger(LogisticsBAreaTDaoImpl.class);


}