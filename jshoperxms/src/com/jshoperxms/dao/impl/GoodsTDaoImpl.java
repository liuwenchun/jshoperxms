package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.GoodsTDao;
import com.jshoperxms.entity.GoodsT;


@Repository("goodsTDao")
public class GoodsTDaoImpl extends BaseTDaoImpl<GoodsT> implements GoodsTDao {


	private static final Logger log = LoggerFactory.getLogger(GoodsTDaoImpl.class);


}