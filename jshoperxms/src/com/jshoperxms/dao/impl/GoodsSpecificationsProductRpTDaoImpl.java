package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.GoodsSpecificationsProductRpTDao;
import com.jshoperxms.entity.GoodsSpecificationsProductRpT;


@Repository("goodsSpecificationsProductRpTDao")
public class GoodsSpecificationsProductRpTDaoImpl extends BaseTDaoImpl<GoodsSpecificationsProductRpT> implements GoodsSpecificationsProductRpTDao{
	
	private static final Logger log = LoggerFactory.getLogger(GoodsSpecificationsProductRpTDaoImpl.class);



}