package com.jshoperxms.dao.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import com.jshoperxms.dao.OrderSnapshotTDao;
import com.jshoperxms.entity.OrderSnapshotT;
@Repository("orderSnapshotTDao")
public class OrderSnapshotTDaoImpl extends BaseTDaoImpl<OrderSnapshotT> implements
		OrderSnapshotTDao {

	private static final Logger log = LoggerFactory.getLogger(OrderSnapshotTDaoImpl.class);


}
