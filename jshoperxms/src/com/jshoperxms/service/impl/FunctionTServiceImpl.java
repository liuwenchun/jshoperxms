package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.FunctionT;
import com.jshoperxms.service.FunctionTService;
@Service("functionMService")
@Scope("prototype")
public class FunctionTServiceImpl extends BaseTServiceImpl<FunctionT> implements
        FunctionTService {


}
