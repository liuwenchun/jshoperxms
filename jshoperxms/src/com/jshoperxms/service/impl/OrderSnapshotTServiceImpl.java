package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.OrderSnapshotT;
import com.jshoperxms.service.OrderSnapshotTService;


@Service("orderSnapshotTService")
@Scope("prototype")
public class OrderSnapshotTServiceImpl extends BaseTServiceImpl<OrderSnapshotT> implements OrderSnapshotTService {

}
