package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.MemberGradeT;
import com.jshoperxms.service.MemberGradeTService;


@Service("memberGradeTService")
@Scope("prototype")
public class MemberGradeTServiceImpl extends BaseTServiceImpl<MemberGradeT>implements MemberGradeTService {



}
