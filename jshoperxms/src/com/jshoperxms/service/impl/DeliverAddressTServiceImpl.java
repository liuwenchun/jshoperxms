package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.DeliverAddressT;
import com.jshoperxms.service.DeliverAddressTService;
@Service("deliverAddressTService")
@Scope("prototype")
public class DeliverAddressTServiceImpl extends BaseTServiceImpl<DeliverAddressT> implements DeliverAddressTService {

}
