package com.jshoperxms.service.impl;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import com.jshoperxms.entity.PostsT;
import com.jshoperxms.service.PostsTService;

@Service("postsTService")
@Scope("prototype")
public class PostsTServiceImpl extends BaseTServiceImpl<PostsT> implements PostsTService {

}
