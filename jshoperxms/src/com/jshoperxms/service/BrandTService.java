package com.jshoperxms.service;


import com.jshoperxms.entity.BrandT;

public interface BrandTService extends BaseTService<BrandT>{

	/**
	 * 保存品牌及商品品牌和商品类型的关系
	 * @param brandT
	 * @param goodsTypeBrandT
	 */
	public boolean saveBrand(BrandT brand,String goodsTypeId,String goodsTypeName);
	
}
