package com.jshoperxms.entity;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


/**
 * The persistent class for the card_t database table.
 * 
 */
@Entity
@Table(name="card_t")
@NamedQuery(name="CardT.findAll", query="SELECT c FROM CardT c")
public class CardT implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private String cardId;

	@Temporal(TemporalType.TIMESTAMP)
	private Date addTime;

	private String amount;

	private String memberID;

	private String password;

	private int removed;

	public CardT() {
	}

	public String getCardId() {
		return this.cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Date getAddTime() {
		return this.addTime;
	}

	public void setAddTime(Date addTime) {
		this.addTime = addTime;
	}

	public String getAmount() {
		return this.amount;
	}

	public void setAmount(String amount) {
		this.amount = amount;
	}

	public String getMemberID() {
		return this.memberID;
	}

	public void setMemberID(String memberID) {
		this.memberID = memberID;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getRemoved() {
		return this.removed;
	}

	public void setRemoved(int removed) {
		this.removed = removed;
	}

}